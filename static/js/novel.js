
var url = location.search;
var page = 1;
var size = 20;
var base_url = "http://glitfc.leanapp.cn";
var page_count;
if (url.indexOf('?') != -1) {
    var str = url.substr(1);
    httpReq("get",base_url + "/book/book_list?"+str);

    console.log(str);
}else{
    httpReq("get",base_url + "/book/book_list");
}
$(document).ready(function () {
    var result_sort = document.getElementsByClassName('result-sort')[0];
    var a = result_sort.getElementsByTagName('a');
    a[1].onclick = function () {
        console.log('以点击率排序');
        a[1].className = "active";
        a[0].className = "";
        httpReq("get", base_url + "/book/book_list?page=" + page + "&size=" + size + "&order_word=Once");
        // a[1].setAttribute("href", "http://" + host  + "/?order_word=Once");
        console.log(a[1]);
    };
    a[0].onclick = function () {
        console.log('以更新时间排序');
        a[0].className = "active";
        a[1].className = "";
        httpReq("get", base_url + "/book/book_list?page=" + page + "&size=" + size);

    };
     getRem(750, 100);
});
$.getJSON(base_url+ "/book/count/book", {
    //参数
}, function (json) {
    page_count = json.data.count;
    return page_count;
});
// page(page_count);
// console.log("page_count:" + page_count);

setTimeout(function () {
    layui.use('laypage', function () {
    var laypage = layui.laypage;

    //执行一个laypage实例
    laypage.render({
        elem: 'pages' //注意，这里的 pages 是 ID，不用加 # 号
        , count: page_count //数据总数，从服务端得到
        , limit: 20
        , theme: 'page'
        , jump: function (obj, first) {
            // console.log(obj.curr);
            // console.log(obj.limit);
            if (!first) {
                var result_sort = document.getElementsByClassName('result-sort')[0];
                var order_active = result_sort.getElementsByClassName("active")[0];
                if (order_active.innerHTML == "总点击量"){
                    httpReq("get",base_url + "/book/book_list?page=" + obj.curr + "&size=" + size + "&order_word=Once");

                }else{
                    httpReq("get",base_url + "/book/book_list?page=" + obj.curr + "&size=" + size);

                }
            }
        }
    });
});
}, 1000);

function selectType(number) {
    var list_type = document.getElementsByClassName("list-type")[0];
    var tags = list_type.getElementsByClassName("tags")[0];
    var span = tags.getElementsByTagName("span");
    $(".data-type").removeClass("active");
    span[number].className = ("active data-type");

    var list_status = document.getElementsByClassName("list-status")[0];
    var status_tags = list_status.getElementsByClassName("tags")[0];
    var status_active = status_tags.getElementsByClassName("active")[0];
    console.log("status_active:" + status_active.innerHTML);
    if (span[number].innerHTML == "全部") {
        if (status_active.innerHTML == "全部"){
            httpReq("get",base_url + "/book/book_list?page=" + page + "&size=" + size);
        }else {
            httpReq("get",base_url + "/book/book_list?page=" + page + "&size=" + size + "&status="+status_active.innerHTML);
        }

    }else if (status_active.innerHTML == "全部"){
        httpReq("get",base_url + "/book/book_list?page=" + page + "&size=" + size + "&type_=" + span[number].innerHTML);

    } else{
        httpReq("get",base_url + "/book/book_list?page=" + page + "&size=" + size + "&type_=" + span[number].innerHTML + "&status=" + status_active.innerHTML);

    }

}


function selectStatus(number) {
    var list_status = document.getElementsByClassName("list-status")[0];
    var tags = list_status.getElementsByClassName("tags")[0];
    var span = tags.getElementsByTagName("span");
    $(".data-status").removeClass("active");
    // span.removeClass();
    span[number].className = ("active data-status");

    var list_type = document.getElementsByClassName("list-type")[0];
    var type_tags = list_type.getElementsByClassName("tags")[0];
    var type_active = type_tags.getElementsByClassName("active")[0];
    console.log("type_active:" + type_active.innerHTML);

    if (span[number].innerHTML == "全部") {
        if (type_active.innerHTML == "全部"){
            httpReq("get",base_url + "/book/book_list?page=" + page + "&size=" + size);

        }else {
            $.getJSON(base_url+ "/book/count/book", {
                //参数
                type_ : type_active
            }, function (json) {
                page_count = json.data.count;
                return page_count;
            });
            page(page_count);
            httpReq("get",base_url + "/book/book_list?page=" + page + "&size=" + size + "&type_=" + type_active.innerHTML);
        }

    }else if (type_active.innerHTML == "全部"){
        httpReq("get",base_url + "/book/book_list?page=" + page + "&size=" + size + "&status="+span[number].innerHTML);

    }else{
        httpReq("get",base_url + "/book/book_list?page=" + page + "&size=" + size + "&status="+span[number].innerHTML + "&type_=" + type_active.innerHTML);
    }
}
function httpReq(method,url) {
    var xmlHttpReq = null;
    //IE浏览器使用ActiveX
    if (window.ActiveXObject) {
        xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //其它浏览器使用window的子对象XMLHttpRequest
    else if (window.XMLHttpRequest) {
        xmlHttpReq = new XMLHttpRequest();
    }
    if (xmlHttpReq != null) {
        //设置请求（没有真正打开），true：表示异步
        // xmlHttpReq.open("GET", , true);
        xmlHttpReq.open(method, url, true);
        //设置回调，当请求的状态发生变化时，就会被调用，传递参数xmlHttpReq
        xmlHttpReq.onreadystatechange = function () {
            console.log(xmlHttpReq.readyState);
            if (xmlHttpReq.readyState === 4) {
                // console.log(xmlHttpReq.readyState);
                onajaxtest(xmlHttpReq);

            }
        };

        //提交请求
    xmlHttpReq.send(null);
    }
    return xmlHttpReq;
}
function onajaxtest(xmlHttpReq) {
    var result_list = document.getElementsByClassName("result-list")[0];
    var ul = result_list.getElementsByTagName("ul")[0];
    var json = JSON.parse(xmlHttpReq.response);
    console.log(json.data);
    if (json.data.length === 0){
        ul.innerHTML = "";
        ul.className = "nodata";
        var li = document.createElement("li");
        ul.appendChild(li);

        var text = document.createTextNode("暂无查找数据");
        li.appendChild(text);

    } else{
        ul.innerHTML = "";
        ul.className = "";
        for (var i = 0; i < json.data.length; i++) {
            var li = document.createElement("li");
            ul.appendChild(li);
            var a = document.createElement("a");
            li.appendChild(a);
            a.setAttribute("href", "/book_info/" + json.data[i].objectId);
            a.setAttribute("target", "_blank");

            var img = document.createElement("img");
            a.appendChild(img);
            img.setAttribute("src", json.data[i].img);
            img.setAttribute("alt", json.data[i].bkname);

            var div = document.createElement("div");
            li.appendChild(div);
            div.className = ("book-info");
            var h4 = document.createElement("h4");
            div.appendChild(h4);
            var a_title = document.createElement("a");
            h4.appendChild(a_title);
            a_title.setAttribute("href", "/book_info/" + json.data[i].objectId);
            a_title.setAttribute("target", "_blank");
            a_title.innerText = json.data[i].bkname;
            var span = document.createElement("span");
            h4.appendChild(span);
            span.className = ("subtitle");
            var a_author = document.createElement("a");
            var a_type = document.createElement("a");
            var a_status = document.createElement("a");
            span.appendChild(a_author);
            span.appendChild(a_type);
            span.appendChild(a_status);
            a_author.setAttribute("target", "_blank");
            a_author.innerText = json.data[i].author;
            a_type.setAttribute("target", "_blank");
            a_type.innerText = json.data[i].type_;
            a_status.setAttribute("target", "_blank");
            a_status.innerText = json.data[i].status;
            var p_intro = document.createElement("p");
            div.appendChild(p_intro);
            p_intro.className = ("book-detail");
            p_intro.innerText = json.data[i].intro;
            var p_num = document.createElement("p");
            div.appendChild(p_num);
            p_num.className = ("book-number");
            var span_click = document.createElement("span");
            var span_updateTime = document.createElement("span");
            p_num.appendChild(span_click);
            p_num.appendChild(span_updateTime);
            span_click.innerText = "总点击量：" + json.data[i].Once;
            span_updateTime.innerText = "更新时间：" + json.data[i].updatedAt.iso;
    }
    }
    }
function jump(){
      var value = document.getElementById("search-text").value;
      console.log(value);
      var resp = httpReq("get",base_url + "/book/book_list?bkname=" + value);
    }
function getRem(pwidth, prem) {
    var html = document.getElementsByTagName("html")[0];
    var oWidth = document.body.clientWidth || document.documentElement.clientWidth;
    html.style.fontSize = oWidth / pwidth * prem + "px";
}