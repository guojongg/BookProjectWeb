base_url = 'http://glitfc.leanapp.cn';
function HttpReq(method, url, fav_id) {
        var xmlHttpReq = null;
        //IE浏览器使用ActiveX
        if (window.ActiveXObject) {
            xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        //其它浏览器使用window的子对象XMLHttpRequest
        else if (window.XMLHttpRequest) {
            xmlHttpReq = new XMLHttpRequest();
        }

        if (xmlHttpReq != null) {
            xmlHttpReq.open(method, url, true);
            xmlHttpReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlHttpReq.setRequestHeader("dataType", "json");
            if (method === "post"){
                xmlHttpReq.onreadystatechange = function () {
                if (xmlHttpReq.readyState === 4) {
                    // console.log(xmlHttpReq);
                    return xmlHttpReq;
                }
            };
                 //提交请求
            xmlHttpReq.send("fav_ids[]=" + fav_id);
            } else {
                 xmlHttpReq.onreadystatechange = function () {
                if (xmlHttpReq.readyState === 4) {
                    // console.log(xmlHttpReq.readyState);
                    onajaxtest(xmlHttpReq);
                    return xmlHttpReq;
                }
            };
                  //提交请求
                xmlHttpReq.send(null);
            }
            // console.log(xmlHttpReq);
            return xmlHttpReq;

        }
}
function onajaxtest(xmlHttpReq) {
     var tfoot = document.getElementsByTagName("tfoot")[0];
     var tbody = document.getElementsByTagName("tbody")[0];
     var json = JSON.parse(xmlHttpReq.response);
     tbody.innerHTML = "";
    if (json.data == undefined){
        HttpReq("get",base_url + "/book_fav/fav/list?user_id=" + user_id);
    } else if (json.data.length === 0){
        var tr = document.createElement("tr");
        tbody.appendChild(tr);
        var td = document.createElement("td");
        tr.appendChild(td);
        td.innerHTML = "暂时没有收藏的数据";
        tr.className = ("nodata");
        td.setAttribute("colspan", "6");
        tfoot.setAttribute("style", "display:none");
    }else {
        for(var i = 0; i<json.data.length; i++){

            var tr = document.createElement("tr");
            tbody.appendChild(tr);
            var td = document.createElement("td");
            tbody.appendChild(td);
            var input = document.createElement("input");
            td.appendChild(input);
            input.setAttribute("type", "checkbox");
            input.setAttribute("value", json.data[i].fav_id);
            input.name = "fav_book";
            input.className = "checked";
            input.setAttribute("onclick", "checkall("+i+")");

            var td = document.createElement("td");
            tbody.appendChild(td);
            var a_classify = document.createElement("a");
            var a_bk_title = document.createElement("a");
            td.appendChild(a_classify);
            a_classify.innerText = "[" + json.data[i].type + "]";
            a_classify.className = "classify";
            a_classify.setAttribute("href", "/index?type_=" + json.data[i].type);
            td.appendChild(a_bk_title);
            a_bk_title.innerText = json.data[i].book_name;
            a_bk_title.className = "bk-title";
            a_bk_title.href = "/book_info/" + json.data[i].book_id;

            var td = document.createElement("td");
            tbody.appendChild(td);
            var a_chapter = document.createElement("a");
            td.appendChild(a_chapter);
            a_chapter.innerText = json.data[i].newzj;
            a_chapter.className = "new-chapter";
            a_chapter.href = "/book_info/" + json.data[i].book_id;

            var td = document.createElement("td");
            tbody.appendChild(td);
            var a_author = document.createElement("a");
            td.appendChild(a_author);
            a_author.innerText = json.data[i].author;
            a_author.className = "author";
            a_author.href = "/book_info/" + json.data[i].book_id;

            var td = document.createElement("td");
            tbody.appendChild(td);
            var span = document.createElement("span");
            td.appendChild(span);
            span.innerText = json.data[i].update_time;
            span.className = "update-time";

            var td = document.createElement("td");
            tbody.appendChild(td);
            var input_del = document.createElement("input");
            td.appendChild(input_del);
            input_del.value = "删除";
            input_del.type = "button";
            input_del.setAttribute("onclick", "favdel(\""+json.data[i].fav_id+"\")");
            input_del.className = "delete";
        }

    }
}
$(document).ready(function () {
    var user_id;
    $.getJSON(base_url + "/user/get_user", {
        //参数
    }, function (json) {
        var code = json.code;
        if (code != 200) {

        } else {
            user_id = json.data.objectId;
            var httpReq = HttpReq("get", base_url + "/book_fav/fav/list?user_id=" + user_id);
        }


    });
    var checkall = document.getElementsByClassName("checkall")[0];
    var checkbox = document.getElementsByClassName("checked");
    checkall.onclick = function () {
        for (var i = 0; i<checkbox.length; i++){
            checkbox[i].checked = this.checked;
        }
        for (var i = 0; i<checkbox.length; i++) {
            checkbox[i].onclick = function () {
                if(!this.checked){
                    checkall.checked = false;
                }

            }
        }
    }

});
function checkall_1(i) {
    var checkall = document.getElementsByClassName("checkall")[0];
    var checkbox = document.getElementsByClassName("checked");
    // for (var i = 0; i<checkbox.length; i++) {
    //         checkbox[i].onclick = function () {
                if(!checkbox[i].checked){
                    console.log(this.checked);
                    checkall.checked = false;
                }else{
                    var num = 0;
                    for (var j = 0; j<checkbox.length; j++){
                        if (checkbox[j].checked){
                            num++;
                            console.log(num);
                        }
                    }
                    console.log(num);
                    if (num === checkbox.length){
                        checkall.checked = true;
                    }
                }

            // }
        // }

}
function checkall(i) {
    var checkall = document.getElementsByClassName("checkall")[0];
    var checkbox = document.getElementsByClassName("checked");

    var num = 0;
    for (var j = 0; j<checkbox.length; j++){
        if (checkbox[j].checked){
            num++;
            console.log(num);
        }
    }
    console.log(num);
    if (num === checkbox.length){
        checkall.checked = true;
    }else{
        checkall.checked = false;
    }

}

function favdel(check) {
    var checkArray = [];
    var checkbox = document.getElementsByClassName("checked");
    for (var i = 0; i< checkbox.length; i++){
        var obj = checkbox[i];
        if (obj.type === "checkbox"){
            if (obj.checked === true) {
                checkArray.push(obj.value);
            }
        }
    }
    if (check !== "" || check !== null || check !== undefined){
        checkArray.push(check);
    }
    if (checkArray.length !== 0){
        $.post(base_url + "/book_fav/fav/del/all", { 'fav_ids[]': checkArray },
            function(data){
                console.log("Data Loaded: " + data.code);
                if (data.code === 200){
                    var user_id;
                    $.getJSON(base_url + "/user/get_user", {
                        //参数
                    }, function (json) {
                        var code = json.code;
                        if (code != 200) {

                        } else {
                            user_id = json.data.objectId;
                        }
                        HttpReq("get", base_url + "/book_fav/fav/list?user_id=" + user_id);
                    });
                } else{
                        alert(data.message);
                }
        });
    }

}